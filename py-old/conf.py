# Layman's Configuration File Reader
# Written by Michael D. Reiley (Seisatsu)
# Copyright (c) 2010, Michael D. Reiley
# - AND -
# Omega Software Development Group (omegadev.org)
# Released under the ISC License
# http://www.isc.org/software/license

import os
from string import lower

def cutout(inputstr, lbound, rbound): # Function to isolate a string
	
	if not inputstr == None:
		if len(lbound) > 0:
			output = inputstr.partition(lbound)[2] # Chop left boundary
		else:
			output = inputstr
		if len(rbound) > 0:
			output = output.partition(rbound)[0] # Chop right boundary
		if not output == None:
			return output
		else:
			return -1
	else:
		return -1

def parse_conf(configfile): # Parse the configuration file
	CONF = {}
	
	if not os.path.isfile(configfile): # Make sure configuration file exists
		return -1
	
	configfile = file(configfile, 'r')
	
	newsection = 0 # Some variables must be initialized
	section = ''
	conf_var = ''
	conf_value = ''
	
	while 1:
		conf_line = configfile.readline()
		if conf_line == '': # End of file?
			break
		if conf_line == '\n': # Empty line?
			continue
		if '#' in conf_line: # Comment?
			comment_ind = 0
			while '\\#' in conf_line: # Strip escapes, count escaped comment chars
				conf_line = conf_line.replace('\\#', '#', 1)
				comment_ind += 1
			conf_line = conf_line.split('#') # Split out the comment chars
			conf_line = '#'.join(conf_line[:comment_ind+1]) # Place back the escaped comment chars
			
			if conf_line == '' or conf_line == '\n': # This line only contains a comment
				continue
		
		conf_line = conf_line.lstrip().rstrip() 
		
		if conf_line == '': # Nothing left
			continue
		
		if '[' in conf_line and ']' in conf_line: # New section
			section = lower(cutout(conf_line, '[', ']'))
			newsection = 1
			if section == -1: # Empty section name
				return -2
		
		if (' ' in conf_line or '\t' in conf_line) and newsection != 1:
			conf_line = conf_line.split(None, 1) # Isolate variable and value
			conf_var = lower(conf_line[0])
			conf_value = conf_line[1]
		
		# Begin filling the CONF dictionary!

		if newsection == 1:
			CONF[section] = {}
		else:
			CONF[section][conf_var] = conf_value
		
		newsection = 0
		
	return CONF

