############################################################
# Tsunagari Tile Engine [client.py]                        #
# Copyright (c) 2010:                                      #
# Paul D. Merrill (pmerrill@omegadev.org)                  #
# Michael D. Reiley (mreiley@omegadev.org)                 #
# Omega Software Development Group (http://omegadev.org/)  #
############################################################
# Released under the ISC License                           #
# (http://www.isc.org/software/license)                    #
# -- NONCOMMERCIAL USE ONLY --                             #
############################################################

from conf import parse_conf

# Echo client program
import socket

HOST = 'localhost'    # The remote host
PORT = 50007              # The same port as used by the server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
s.send('Hello, world')
data = s.recv(1024)
s.close()
print 'Received', repr(data)
