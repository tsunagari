############################################################
# Tsunagari Tile Engine [server.py]                        #
# Copyright (c) 2010:                                      #
# Paul D. Merrill (pmerrill@omegadev.org)                  #
# Michael D. Reiley (mreiley@omegadev.org)                 #
# Omega Software Development Group (http://omegadev.org/)  #
############################################################
# Released under the ISC License                           #
# (http://www.isc.org/software/license)                    #
# -- NONCOMMERCIAL USE ONLY --                             #
############################################################

# Echo server program
import socket

HOST = ''                 # Symbolic name meaning all available interfaces
PORT = 50007              # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
print 'Connected by', addr
while 1:
    data = conn.recv(1024)
    if not data: break
    conn.send(data)
conn.close()
