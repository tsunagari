############################################################
# Tsunagari Tile Engine [tpkg.py]                          #
# Copyright (c) 2010:                                      #
# Paul D. Merrill (pmerrill@omegadev.org)                  #
# Michael D. Reiley (mreiley@omegadev.org)                 #
# Omega Software Development Group (http://omegadev.org/)  #
############################################################
# Released under the ISC License                           #
# (http://www.isc.org/software/license)                    #
# -- NONCOMMERCIAL USE ONLY --                             #
############################################################

import urllib, sys, tarfile

def tpkg_update(name, CONF):
	"""Update or install a TPKG from the game's FTP repository."""
	try:
		remote_file = urllib.urlopen(CONF['network']['repobase']+'tpkg/'+name)
		local_file = open(CONF['game']['datadir']+name, 'w')
		local_file.write(remote_file.read())
		remote_file.close()
		local_file.close()
		return 0
	except IOError:
		return -1

def tpkg_open(name, CONF):

def tpkg_read_info(tpkg):
	pass

def tpkg_read_file(tpkg, filename):
	pass

